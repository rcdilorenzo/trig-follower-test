//
//  main.m
//  Follower
//
//  Created by Christian Di Lorenzo on 6/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LDNAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LDNAppDelegate class]));
    }
}
