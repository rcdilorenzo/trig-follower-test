//
//  ArrowPointer.h
//  Follower
//
//  Created by Christian Di Lorenzo on 6/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArrowPointer : UIImageView

- (void)updateFromPoint:(CGPoint)point;

@end
