//
//  LDNViewController.m
//  Follower
//
//  Created by Christian Di Lorenzo on 6/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import "LDNViewController.h"
#import "ArrowPointer.h"

@interface LDNViewController ()
@end

@implementation LDNViewController

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self notifyArrowsFromEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    [self notifyArrowsFromEvent:event];
}

- (void)notifyArrowsFromEvent:(UIEvent *)event {
    CGPoint location = [[event allTouches].anyObject locationInView:self.view];
    for (UIView *subview in self.view.subviews) {
        if ([subview isKindOfClass:[ArrowPointer class]]) {
            [(ArrowPointer *)subview updateFromPoint:location];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
