//
//  ArrowPointer.m
//  Follower
//
//  Created by Christian Di Lorenzo on 6/1/13.
//  Copyright (c) 2013 Light Design. All rights reserved.
//

#import "ArrowPointer.h"
#import <QuartzCore/QuartzCore.h>

#define ToRad(deg) 		( (M_PI * (deg)) / 180.0 )
#define ToDeg(rad)		( (180.0 * (rad)) / M_PI )
#define OFFSET 90

@interface ArrowPointer()

@end


@implementation ArrowPointer

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.image = [UIImage imageNamed:@"arrow.png"];
    }
    return self;
}

- (id)init {
    self = [super initWithImage:[UIImage imageNamed:@"arrow.png"]];
    return self;
}

- (void)updateFromGesture:(UIGestureRecognizer *)gesture {
    [self updateFromPoint:[gesture locationInView:self.superview]];
}

float relativeAngle(CGPoint p1, CGPoint p2) {
    float result = ToDeg(atan2(p2.y-p1.y, p2.x-p1.x));
    return (result >=0  ? result : result + 360.0);
}

- (void)updateFromPoint:(CGPoint)point {
    [UIView animateWithDuration:0.3 animations:^{
        float angle = relativeAngle(self.center, point)+OFFSET;
        self.transform = CGAffineTransformMakeRotation(ToRad(angle));
        
        float xDelta = cos(angle), yDelta = sin(angle);
//        self.center = CGPointMake(self.center.x+xDelta*2, self.center.y+yDelta*2);
    }];
}

@end
